#!/usr/bin/env ruby
# coding: utf-8
# frozen_string_literal: true

FILE_ZAPSANI = './data/zapsani.txt'.freeze
FILE_PREZENCKA = './data/prezencka-poznamkovy-blok.txt'.freeze

require 'set'
require 'iconv'

# Class for Student
class Student
  attr_reader :name, :uco
  def initialize(name, uco)
    @name = name
    @uco = uco
  end

  def name_translit
    Iconv.conv('ascii//translit', 'utf-8', name)
  end

  def to_s
    "#{name} (#{uco})"
  end
end

# Class for Klass
class Klass
  attr_reader :students

  def self.instance
    @instance ||= Klass.load(FILE_ZAPSANI)
  end

  def self.load(file)
    students = File.read(file).lines.map do |line|
      uco, name = line.match(/(\d{6})\t(.*)\t(FI|ESF)/)&.captures
      Student.new(name, uco) if uco
    end.compact
    new(students)
  end

  def initialize(students)
    @students = students
  end

  def find_students(str)
    students.find_all do |s|
      if /\A\d*\Z/.match?(str)
        s.uco.include?(str)
      else
        s.name_translit.downcase.include?(str.downcase)
      end
    end
  end
end

# Class for Prezencka
class Prezencka
  attr_reader :present_students

  def initialize
    @present_students = Set.new
  end

  def fill
    puts "Enter part of učo or name of student ((s) for quit and save, (q) for quit without save)"
    while (input = gets&.chomp)
      case input
      when "q"
        return false
      when "s"
        return true
      else
        fill_input(input)
      end
    end
  end

  # rubocop:disable Metrics/MethodLength
  def fill_input(input)
    return unless (s = Klass.instance.find_students(input))

    case s.size
    when 0
      puts "Student for '#{input}' not found"
    when 1
      puts "Found student #{s.first}"
      @present_students << s.first
    else
      puts "Found students\n\n#{s.map(&:to_s).join("\n")}\n\n provide" \
           'more details'
    end
  end
  # rubocop:enable Metrics/MethodLength

  def inspect
    "@present_students: #{@present_students.inspect}"
  end
end

# class for PoznamkovyBlok
class PoznamkovyBlok
  def initialize
    @prezencky = []
  end

  def add_prezencka
    prezencka = Prezencka.new
    @prezencky << prezencka
    prezencka
  end

  def export
    Klass.instance.students.map do |student|
      parts = ["#{student.uco}/PV249/570837"]
      parts << @prezencky.map do |p|
        p.present_students.include?(student) ? '*1' : '*0'
      end.join(' | ')
      parts.join(':')
    end.join("\n")
  end

  def load_from_file
    return unless File.exist? FILE_PREZENCKA
    File.read(FILE_PREZENCKA).lines.each do |line|
      uco, points = line.match(/\A(\d{6}).*:(.*)\Z/).captures
      students = Klass.instance.find_students(uco)
      raise "Studnet #{uco} not found" if students.empty?

      points.split("|").each.with_index do |part, i|
        @prezencky[i] ||= Prezencka.new
        @prezencky[i].present_students << students.first if part.include?('1')
      end
    end
    puts "Prezencka loaded from #{FILE_PREZENCKA}"
  end

  def export_to_file
    File.write(FILE_PREZENCKA, export)
  end
end


blok = PoznamkovyBlok.new
blok.load_from_file

prezencka = blok.add_prezencka
if prezencka.fill
  puts "Saving to #{FILE_PREZENCKA}"
  blok.export_to_file
else
  puts "Quit without save"
end
