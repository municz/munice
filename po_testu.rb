#!/usr/bin/env ruby
# coding: utf-8
# frozen_string_literal: true

require 'pry'

FILE_VYSLEDKY = './data/test-vysledky.txt'.freeze
FILE_MAIL = './data/uvodni-mail.txt'.freeze
FILE_ZAPSANI = './data/zapsani.txt'.freeze
FILE_VYJIMKY = './data/vyjimky.txt'.freeze
FILE_VYRIZENI = './data/vyrizeni.txt'.freeze
FILE_POZNAMOKOVY_BLOK = './data/poznamkovy-blok.txt'.freeze

def zapsani
  File.read(FILE_ZAPSANI).lines.map { |line| line.split("\t")[1] }
end

def s_testem
  File.read(FILE_VYSLEDKY).lines.map { |line| line.split(" ").first }
end

def s_uvodnim_mailem
  File.read(FILE_MAIL).lines.map { |line| line[/\d{6}/] }
end

def vyjimky
  File.read(FILE_VYJIMKY).lines.map { |line| line[/učo (\d{6})/,1] }.compact
end

def vyrizeni
  File.read(FILE_VYRIZENI).lines.map(&:chomp)
end

def pozvat_na_opravu
  zapsani - (s_uvodnim_mailem + s_testem)
end

def udelit_vyjimku
  vyjimky & s_testem - vyrizeni
end

def napsat_o_vyjimku
  s_testem - vyjimky
end

def format_email(ucos)
  return 'nikdo' if ucos.empty?
  ucos.map { |uco| "#{uco}@mail.muni.cz" }.join(", ")
end

def generuj_poznamkovy_blok
  zapsani = self.zapsani
  content = File.read(FILE_VYSLEDKY).lines.map do |line|
    uco, body = line.split(' ')
    next unless zapsani.include?(uco)
    "#{uco}/PV249/570837:*#{body}"
  end.compact.join("\n")
  File.write(FILE_POZNAMOKOVY_BLOK, content)
end

puts "Poslat pripominku o zadosti o vyjimku: #{format_email(napsat_o_vyjimku)}"
puts "Poslat info o nahradním testu: #{format_email(pozvat_na_opravu)}"
puts "Udelit vyjimku: #{udelit_vyjimku}"

