# MUNIce

## kanon na vrabce

Sada skriptů na podporu agendy předmětu PV249: Programování v jazyce Ruby.
Repositář taky slouží k demonstračním účelům jazyka Ruby.

## použití

```
bundle install

# výpis akcí na vykonání po dokončení úvodního testu
./po_testu.rb

# zápis prezenčky
./prezence.rb
```
